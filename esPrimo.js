function esPrimo(n){

    let i;
    let a;
    let primo=true;

    i=2;

    while(i<n && primo===false){

        a=n%i;
        if(a===0){
            primo=false;
        }

        i++;
    }
    return primo;

}
